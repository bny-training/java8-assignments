/**
 * 
 */
package interfaces;

/**
 * @author shubham.kushwah
 *
 */
@FunctionalInterface
public interface IsSeqFound {
public boolean checkSeq(String str);
}
