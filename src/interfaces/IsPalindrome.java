/**
 * 
 */
package interfaces;

/**
 * @author shubham.kushwah
 *
 */
@FunctionalInterface
public interface IsPalindrome{
	public boolean checkNumberOps(int num);
}