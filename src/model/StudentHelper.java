/**
 * 
 */
package model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shubham.kushwah
 *
 */
public class StudentHelper {
private List<Student> lstStudent;

/**
 * 
 */
public StudentHelper() {
	super();
	List<Student> lst = new ArrayList<>();
	lst.add(new Student(101,LocalDateTime.of(2022, 1, 14, 12, 34),LocalDateTime.of(2005, 1, 14, 12, 34),"88","56","78","43","63","Eleven"));
	lst.add(new Student(108,LocalDateTime.of(2022, 6, 14, 12, 34),LocalDateTime.of(2004, 10, 14, 12, 34),"48","26","38","33","21","Eleven"));
	lst.add(new Student(201,LocalDateTime.of(2022, 4, 14, 12, 34),LocalDateTime.of(2003, 11, 14, 12, 34),"78","96","32","40","87","Tweleve"));
	lst.add(new Student(109,LocalDateTime.of(2022, 3, 14, 12, 34),LocalDateTime.of(2005, 3, 14, 12, 34),"98","66","68","41","99","Eleven"));
	lst.add(new Student(205,LocalDateTime.of(2022, 5, 14, 12, 34),LocalDateTime.of(2003, 7, 14, 12, 34),"58","76","75","89","53","Tweleve"));
	setLstStudent(lst);
}

/**
 * @return the lstStudent
 */
public List<Student> getLstStudent() {
	return lstStudent;
}

/**
 * @param lstStudent the lstStudent to set
 */
public void setLstStudent(List<Student> lstStudent) {
	this.lstStudent = lstStudent;
}

}
