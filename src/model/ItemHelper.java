/**
 * 
 */
package model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shubham.kushwah
 *
 */
public class ItemHelper {
List<Item> lst;


/**
 * 
 */
public ItemHelper() {
	super();
	List<Item> lstItems = new ArrayList<>();
	lstItems.add(new Item(1001,"milk",LocalDateTime.of(2022, 1, 14, 12, 34),LocalDateTime.of(2022, 05, 14, 12, 34),2093));
	lstItems.add(new Item(1002,"water bottle",LocalDateTime.of(2022, 05, 14, 12, 34),null,550));
	lstItems.add(new Item(1003,"dresses",LocalDateTime.of(2022, 07, 23, 12, 34),null,6550));
	lstItems.add(new Item(1004,"soap",LocalDateTime.of(2022, 6, 29, 12, 34),LocalDateTime.of(2023, 05, 14, 12, 34),2093));
	lstItems.add(new Item(1005,"dolo 650",LocalDateTime.of(2022, 03, 18, 11, 34),LocalDateTime.of(2024, 05, 01, 12, 34),8286));
	setLst(lstItems);
}


/**
 * @return the lst
 */
public List<Item> getLst() {
	return lst;
}

/**
 * @param lst the lst to set
 */
public void setLst(List<Item> lst) {
	this.lst = lst;
}

}
