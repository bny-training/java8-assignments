/**
 * 
 */
package model;

import java.time.LocalDateTime;

/**
 * @author shubham.kushwah
 *
 */
public class Student {
	int rollno;
	LocalDateTime date_of_addmission,dob;
	String maths, physics, chemistry, English, hindi; // marks in subjects
	String classname;
	/**
	 * @return the rollno
	 */
	public int getRollno() {
		return rollno;
	}
	/**
	 * @param rollno the rollno to set
	 */
	public void setRollno(int rollno) {
		this.rollno = rollno;
	}
	/**
	 * @return the date_of_addmission
	 */
	public LocalDateTime getDate_of_addmission() {
		return date_of_addmission;
	}
	/**
	 * @param date_of_addmission the date_of_addmission to set
	 */
	public void setDate_of_addmission(LocalDateTime date_of_addmission) {
		this.date_of_addmission = date_of_addmission;
	}
	/**
	 * @return the dob
	 */
	public LocalDateTime getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(LocalDateTime dob) {
		this.dob = dob;
	}
	/**
	 * @return the maths
	 */
	public String getMaths() {
		return maths;
	}
	/**
	 * @param maths the maths to set
	 */
	public void setMaths(String maths) {
		this.maths = maths;
	}
	/**
	 * @return the physics
	 */
	public String getPhysics() {
		return physics;
	}
	/**
	 * @param physics the physics to set
	 */
	public void setPhysics(String physics) {
		this.physics = physics;
	}
	/**
	 * @return the chemistry
	 */
	public String getChemistry() {
		return chemistry;
	}
	/**
	 * @param chemistry the chemistry to set
	 */
	public void setChemistry(String chemistry) {
		this.chemistry = chemistry;
	}
	/**
	 * @return the english
	 */
	public String getEnglish() {
		return English;
	}
	/**
	 * @param english the english to set
	 */
	public void setEnglish(String english) {
		English = english;
	}
	/**
	 * @return the hindi
	 */
	public String getHindi() {
		return hindi;
	}
	/**
	 * @param hindi the hindi to set
	 */
	public void setHindi(String hindi) {
		this.hindi = hindi;
	}
	/**
	 * @return the classname
	 */
	public String getClassname() {
		return classname;
	}
	/**
	 * @param classname the classname to set
	 */
	public void setClassname(String classname) {
		this.classname = classname;
	}
	/**
	 * @param rollno
	 * @param date_of_addmission
	 * @param dob
	 * @param maths
	 * @param physics
	 * @param chemistry
	 * @param english
	 * @param hindi
	 * @param classname
	 */
	public Student(int rollno, LocalDateTime date_of_addmission, LocalDateTime dob, String maths, String physics,
			String chemistry, String english, String hindi, String classname) {
		super();
		this.rollno = rollno;
		this.date_of_addmission = date_of_addmission;
		this.dob = dob;
		this.maths = maths;
		this.physics = physics;
		this.chemistry = chemistry;
		English = english;
		this.hindi = hindi;
		this.classname = classname;
	}
	@Override
	public String toString() {
		return "Student [rollno=" + rollno + ", date_of_addmission=" + date_of_addmission + ", dob=" + dob + ", maths="
				+ maths + ", physics=" + physics + ", chemistry=" + chemistry + ", English=" + English + ", hindi="
				+ hindi + ", classname=" + classname + "]";
	}
	
}
