/**
 * 
 */
package model;

import java.util.ArrayList;
import java.util.List;

import main.City;
import main.State;

/**
 * @author shubham.kushwah
 *
 */
public class CityHelper {
private List<City> cityList;

/**
 * 
 */
public CityHelper() {
	super();
	List<City> lst = new ArrayList<>();
	lst.add(new City(1001,"Khargone",new State(101,"Madhya Pradesh"),30.4f,423525,525));
	lst.add(new City(1002,"Indore",new State(101,"Madhya Pradesh"),40.4f,1123556,1525));
	lst.add(new City(2003,"Hyderabad",new State(201,"Telangana State"),50.2f,923525,2525));
	lst.add(new City(4001,"Ahmedabad",new State(401,"Gujarat"),48.9f,1023525,2422));
	lst.add(new City(5002,"Pune",new State(501,"Maharashtra"),68.9f,1223525,3422));
	this.setCityList(lst);

}

/**
 * @return the cityList
 */
public List<City> getCityList() {
	return cityList;
}

/**
 * @param cityList the cityList to set
 */
public void setCityList(List<City> cityList) {
	this.cityList = cityList;
}

}
