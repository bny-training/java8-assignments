/**
 * 
 */
package model;

import java.time.LocalDateTime;

/**
 * @author shubham.kushwah
 *
 */
public class Item {
	private int itemid; 
	private String iname; 
	private LocalDateTime date_of_manufacturing, date_of_expiry;
	private float price;
	
	/**
	 * @param itemid
	 * @param iname
	 * @param date_of_manufacturing
	 * @param date_of_expiry
	 * @param price
	 */
	public Item(int itemid, String iname, LocalDateTime date_of_manufacturing, LocalDateTime date_of_expiry,
			float price) {
		super();
		this.itemid = itemid;
		this.iname = iname;
		this.date_of_manufacturing = date_of_manufacturing;
		this.date_of_expiry = date_of_expiry;
		this.price = price;
	}
	/**
	 * @return the itemid
	 */
	public int getItemid() {
		return itemid;
	}
	/**
	 * @param itemid the itemid to set
	 */
	public void setItemid(int itemid) {
		this.itemid = itemid;
	}
	/**
	 * @return the iname
	 */
	public String getIname() {
		return iname;
	}
	/**
	 * @param iname the iname to set
	 */
	public void setIname(String iname) {
		this.iname = iname;
	}
	/**
	 * @return the date_of_manufacturing
	 */
	public LocalDateTime getDate_of_manufacturing() {
		return date_of_manufacturing;
	}
	/**
	 * @param date_of_manufacturing the date_of_manufacturing to set
	 */
	public void setDate_of_manufacturing(LocalDateTime date_of_manufacturing) {
		this.date_of_manufacturing = date_of_manufacturing;
	}
	/**
	 * @return the date_of_expiry
	 */
	public LocalDateTime getDate_of_expiry() {
		return date_of_expiry;
	}
	/**
	 * @param date_of_expiry the date_of_expiry to set
	 */
	public void setDate_of_expiry(LocalDateTime date_of_expiry) {
		this.date_of_expiry = date_of_expiry;
	}
	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(float price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Item [itemid=" + itemid + ", iname=" + iname + ", date_of_manufacturing=" + date_of_manufacturing
				+ ", date_of_expiry=" + date_of_expiry + ", price=" + price + "]";
	}

}
