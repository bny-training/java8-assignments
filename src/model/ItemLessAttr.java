/**
 * 
 */
package model;

/**
 * @author shubham.kushwah
 *
 */
public class ItemLessAttr {
	private String iname; 
	private float price;
	/**
	 * @return the iname
	 */
	public String getIname() {
		return iname;
	}
	/**
	 * @param iname the iname to set
	 */
	public void setIname(String iname) {
		this.iname = iname;
	}
	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(float price) {
		this.price = price;
	}
	
}
