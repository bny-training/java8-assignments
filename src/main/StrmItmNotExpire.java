/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import model.Item;
import model.ItemHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmItmNotExpire {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		ItemHelper itmHelper = new ItemHelper();
		List<Item> lst = itmHelper.getLst();
		Comparator<Item> maxItmCmp = Comparator.comparing(Item::getPrice);
		Predicate<Item> p = i->Objects.isNull(i.getDate_of_expiry());
		Item highPrice = lst.stream().filter(p).max(maxItmCmp).get();
		System.out.println("Item with Highest Price without Expiry :: "+highPrice);
	
	
	}

}
