/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;

import model.CityHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmCitylowPollutionArea {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		CityHelper cityHelper = new CityHelper();
		List<City> lstcity = cityHelper.getCityList();
		Comparator<City> maxcityCmp = (city1,city2)->{
			if((city1.getPollutionIndex() < city2.getPollutionIndex())&&(city1.getArea_of_city() < city2.getArea_of_city())) {
				return -1;
			}else if((city1.getPollutionIndex() > city2.getPollutionIndex())&&(city1.getArea_of_city() > city2.getArea_of_city())) {
				return 1;
			}else {
				return 0;
			}

		} ;
		lstcity.stream().sorted(maxcityCmp).limit(1).forEach(i->System.out.println("City with lowest pollution_index and lowest area:: "+i));

	
	}

}
