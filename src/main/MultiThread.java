/**
 * 
 */
package main;

/**
 * @author shubham.kushwah
 *
 */
public class MultiThread extends Thread{
	 int number=500;
	public void reduceNum() {

		for(int i=0;i<100;i++) {
			number = number - (7*number)/100;
			System.out.println("Reduced by 7 % "+number);
		}

	}
	public void increaseNum() {
		System.out.println("==============================");
		for(int i=0;i<100;i++) {
			number = number + number/10;
			System.out.println("Increased by 10 % "+number);
		}

	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		MultiThread mt1 = new MultiThread();
		
		Runnable rn1 = ()->{
//			while(true) {
			mt1.increaseNum();
//			}
			};
		Thread t1 = new Thread(rn1);
		t1.start();
		Runnable rn2 = ()->{
//			while(true) {
			mt1.reduceNum();
//			}
			};
		Thread t2 = new Thread(rn2);
		t2.start();
		
		
		
	}

}
