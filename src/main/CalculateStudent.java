/**
 * 
 */
package main;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import model.Student;

/**
 * @author shubham.kushwah
 *
 */
public class CalculateStudent {
public double calcPercentage(Student std) {
	int total = calcSum(std);
	double per = (total*100)/500;
	return per;
}
public Integer calcSum(Student std) {
	
	return Integer.parseInt(std.getChemistry()) + Integer.parseInt(std.getEnglish()) + Integer.parseInt(std.getHindi()) + Integer.parseInt(std.getMaths()) + Integer.parseInt(std.getPhysics());
}
public List<Integer> toList(Student st, Comparator<Integer> cmp) {
	List<Integer> marks = Arrays.asList(Integer.parseInt(st.getChemistry()),Integer.parseInt(st.getEnglish()),Integer.parseInt(st.getHindi()),Integer.parseInt(st.getMaths()),Integer.parseInt(st.getPhysics()));
	marks.sort(cmp);
	return marks;
}
}
