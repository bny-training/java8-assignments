/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;

import model.CityHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmCityHighLowArea {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CityHelper cityHelper = new CityHelper();
		List<City> lstcity = cityHelper.getCityList();
		Comparator<City> maxcityCmp = (city1,city2)->{
			if((city1.getArea_of_city() < city2.getArea_of_city())&&(city1.getPopulation() > city2.getPopulation())) {
				return 1;
			}else if(city1.getArea_of_city() > city2.getArea_of_city()&&(city1.getPopulation() < city2.getPopulation())) {
				return -1;
			}else {
				return 0;
			}

		} ;
		
		City resCity = lstcity.stream().min(maxcityCmp).get();
		System.out.println("City with less area and highest population :: "+resCity);
	}

}
