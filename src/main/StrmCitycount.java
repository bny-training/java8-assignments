/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import model.CityHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmCitycount {

	/**
	 * @param args
	 */
	public static void main(String[] args) {



		CityHelper cityHelper = new CityHelper();
		List<City> lstcity = cityHelper.getCityList();
		Comparator<City> maxcityCmp = (city1,city2)->{
			if((city1.getObjState().getStateid() == city2.getObjState().getStateid())) {
				return -1;
			}else if((city1.getObjState().getStateid() > city2.getObjState().getStateid())) {
				return -1;
			}else {
				
				return 0;
			}

		} ;
		Function<City, Integer> fn = l->{
			return 1;
		};
//		lstcity.stream().sorted(maxcityCmp).limit(1).forEach(i->System.out.println("City with lowest pollution_index and lowest area:: "+i));
List<Integer> strm =  lstcity.stream().map(i->i.getObjState().getStateid()).distinct().collect(Collectors.toList());
Long cnt = lstcity.stream().filter(i->strm.contains(i.getObjState().getStateid())).mapToInt(i->i.getObjState().getStateid()).summaryStatistics().getSum(); //map(n-> );
System.out.println(cnt);
//	lstcity.stream().collect(Collectors.groupingBy(i->i.getObjState().getStateid()));
	
	}

}
