/**
 * 
 */
package main;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import model.Item;
import model.ItemHelper;

/**
 * @author shubham.kushwah
 *
 */
interface IStrmFilter{
	public boolean checkPrice(Item itm1,Item itm2);
}
public class StrmItmRmvDupPrice {

	/**
	 * @param args
	 */
	public static void main(String[] args) {



		ItemHelper itmHelper = new ItemHelper();
		List<Item> lst = itmHelper.getLst();
		System.out.println("All items :: "+lst);

//List<Item> afterRmv=lst.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparingDouble(Item::getPrice))),
//        ArrayList::new));
//lst.stream().filter(i->itmSet.add(i)).collect(Collectors.)
//afterRmv.forEach(i->System.out.println(i));
//System.out.println("All items after removing duplicate price:: "+afterRmv);
	
Set<Float> itmSet = new HashSet<>();
Predicate<Item> fn = itm->{
	
	return itmSet.add(itm.getPrice());
};
List<Item> afterRmv = lst.stream().filter(fn).collect(Collectors.toList());
afterRmv.forEach(i->System.out.println(i));
	}

}
