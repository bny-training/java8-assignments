/**
 * 
 */
package main;

/**
 * @author shubham.kushwah
 *
 */
public class State {
	private int stateid;
	private String statename;
	
	/**
	 * @return the stateid
	 */
	public int getStateid() {
		return stateid;
	}
	/**
	 * @param stateid the stateid to set
	 */
	public void setStateid(int stateid) {
		this.stateid = stateid;
	}
	/**
	 * @return the statename
	 */
	public String getStatename() {
		return statename;
	}
	/**
	 * @param statename the statename to set
	 */
	public void setStatename(String statename) {
		this.statename = statename;
	}
	/**
	 * @param stateid
	 * @param statename
	 */
	public State(int stateid, String statename) {
		super();
		this.stateid = stateid;
		this.statename = statename;
	}

}
