/**
 * 
 */
package main;

import java.util.Optional;

/**
 * @author shubham.kushwah
 *
 */
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		  

        String[] str = new String[5]; 
  
        Optional<String> checkNull = Optional.ofNullable(str[4]); 
  
        if(checkNull.isPresent()){  

            String lowercaseString = str[5].toLowerCase(); 
            System.out.print(lowercaseString);  

        }else
            System.out.println("string value is not present");  
  
   
	}

}
