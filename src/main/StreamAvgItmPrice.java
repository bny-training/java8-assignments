/**
 * 
 */
package main;

import java.util.List;
import java.util.stream.Collectors;

import model.Item;
import model.ItemHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StreamAvgItmPrice {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ItemHelper itmHelper = new ItemHelper();
		List<Item> lst = itmHelper.getLst();
		double avg = lst.stream().collect(Collectors.averagingDouble(n->n.getPrice()));
		System.out.println("Average price of the Items :: "+avg);
	}

}
