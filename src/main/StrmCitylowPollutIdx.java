/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;

import model.CityHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmCitylowPollutIdx {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		CityHelper cityHelper = new CityHelper();
		List<City> lstcity = cityHelper.getCityList();
		Comparator<City> maxcityCmp = (city1,city2)->{
			if((city1.getPollutionIndex() > city2.getPollutionIndex())) {
				return 1;
			}else if((city1.getPollutionIndex() < city2.getPollutionIndex())) {
				return -1;
			}else {
				return 0;
			}

		} ;
		System.out.println("Citie in order of lowest pollution_index ::\n");
		lstcity.stream().sorted(maxcityCmp).forEach(System.out::println);

	
	}

}
