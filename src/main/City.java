/**
 * 
 */
package main;

/**
 * @author shubham.kushwah
 *
 */
public class City {
	private int cityid;
	private String cityname;
	private State objState;
	private Float pollutionIndex;
	private int population;
	private int area_of_city;
	/**
	 * @return the cityid
	 */
	public int getCityid() {
		return cityid;
	}
	/**
	 * @param cityid the cityid to set
	 */
	public void setCityid(int cityid) {
		this.cityid = cityid;
	}
	/**
	 * @return the cityname
	 */
	public String getCityname() {
		return cityname;
	}
	/**
	 * @param cityname the cityname to set
	 */
	public void setCityname(String cityname) {
		this.cityname = cityname;
	}
	/**
	 * @return the objState
	 */
	public State getObjState() {
		return objState;
	}
	/**
	 * @param objState the objState to set
	 */
	public void setObjState(State objState) {
		this.objState = objState;
	}
	/**
	 * @return the pollutionIndex
	 */
	public Float getPollutionIndex() {
		return pollutionIndex;
	}
	/**
	 * @param pollutionIndex the pollutionIndex to set
	 */
	public void setPollutionIndex(Float pollutionIndex) {
		this.pollutionIndex = pollutionIndex;
	}
	/**
	 * @return the population
	 */
	public int getPopulation() {
		return population;
	}
	/**
	 * @param population the population to set
	 */
	public void setPopulation(int population) {
		this.population = population;
	}
	/**
	 * @return the area_of_city
	 */
	public int getArea_of_city() {
		return area_of_city;
	}
	/**
	 * @param area_of_city the area_of_city to set
	 */
	public void setArea_of_city(int area_of_city) {
		this.area_of_city = area_of_city;
	}
	/**
	 * @param cityid
	 * @param cityname
	 * @param objState
	 * @param pollutionIndex
	 * @param population
	 * @param area_of_city
	 */
	public City(int cityid, String cityname, State objState, Float pollutionIndex, int population, int area_of_city) {
		super();
		this.cityid = cityid;
		this.cityname = cityname;
		this.objState = objState;
		this.pollutionIndex = pollutionIndex;
		this.population = population;
		this.area_of_city = area_of_city;
	}
	@Override
	public String toString() {
		return "City [cityid=" + cityid + ", cityname=" + cityname + ", objStateId=" + objState.getStateid() + ",objStateName=" + objState.getStatename() + ", pollutionIndex="
				+ pollutionIndex + ", population=" + population + ", area_of_city=" + area_of_city + "]";
	}
	
}
