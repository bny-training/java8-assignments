/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;

import model.Item;
import model.ItemHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StreamItmLowPrice {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ItemHelper itmHelper = new ItemHelper();
		List<Item> lst = itmHelper.getLst();
		Comparator<Item> maxItmCmp = Comparator.comparing(Item::getPrice);
		Item lowPrice = lst.stream().min(maxItmCmp).get();
		System.out.println("Item with Lowest Price :: "+lowPrice);
	
	}

}
