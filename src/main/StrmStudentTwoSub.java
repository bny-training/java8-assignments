/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import model.Student;
import model.StudentHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmStudentTwoSub {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		StudentHelper studentHelper = new StudentHelper();
		List<Student> lsList = studentHelper.getLstStudent();
		CalculateStudent calculateStudent = new CalculateStudent();
		Comparator<Integer> cmp = (i1,i2)->{
			return i1>i2?-1:i1<i2?1:0;
		};
		
		Predicate<Student> predMark = st->{
			List<Integer> lstMarks = calculateStudent.toList(st,cmp);
			int cnt =0;
			for(Integer i : lstMarks) {
				if(i<=40) {
					cnt++;
				}
			}
			return cnt==2?true:false;
		};
		
		System.out.println("Number of students need to give exam in two subjects to promoted to another class");
		long cnt = lsList.stream().filter(predMark).count();
		System.out.println(cnt);
	
	
	}

}
