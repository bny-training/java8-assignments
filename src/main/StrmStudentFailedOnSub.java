/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import model.Student;
import model.StudentHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmStudentFailedOnSub {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		StudentHelper studentHelper = new StudentHelper();
		List<Student> lsList = studentHelper.getLstStudent();
		CalculateStudent calculateStudent = new CalculateStudent();
		Comparator<Integer> cmp = (i1,i2)->{
			return i1>i2?-1:i1<i2?1:0;
		};
		
		Predicate<Student> pred = st->{
			List<Integer> lstMarks = calculateStudent.toList(st,cmp);
			
			for(Integer i : lstMarks) {
				if(i<=40) {
					return true;
				}
			}
			return false;
		};
		System.out.println("Students who are fail(<=40) in at least one subject");
		lsList.stream().filter(pred).forEach(System.out::println);

	
	
	}

}
