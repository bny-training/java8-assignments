/**
 * 
 */
package main;

/**
 * @author shubham.kushwah
 *
 */
public class StringIntoString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String str = "InsertOneString";
        String insStr = "intoOtherString";
        int idx = 4;
        StringBuffer newStr
        = new StringBuffer(str);
        System.out.println("After Modification new string :: "+newStr.insert(idx + 1, insStr));
	}

}
