/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import model.Student;
import model.StudentHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmStudentPromoted {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		StudentHelper studentHelper = new StudentHelper();
		List<Student> lsList = studentHelper.getLstStudent();
		CalculateStudent calculateStudent = new CalculateStudent();
		Comparator<Integer> cmp = (i1,i2)->{
			return i1>i2?-1:i1<i2?1:0;
		};
		Predicate<Student> predPer = st->{
			return calculateStudent.calcPercentage(st)<40?true:false;
		};
		Predicate<Student> predMark = st->{
			List<Integer> lstMarks = calculateStudent.toList(st,cmp);
			
			for(Integer i : lstMarks) {
				if(i<=40) {
					return true;
				}
			}
			return false;
		};
		Predicate<Student> test = stu->{
			if(predMark.test(stu) && predPer.test(stu)) {
				return false;
			}else {
				return true;
			}
		};
		System.out.println("Number of Students promoted to another class");
		long cnt = lsList.stream().filter(test).count();
		System.out.println(cnt);
	
	}

}
