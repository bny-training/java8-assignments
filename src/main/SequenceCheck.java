/**
 * 
 */
package main;

import java.util.function.Predicate;

/**
 * @author shubham.kushwah
 *
 */
public class SequenceCheck {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s = "Taj is situated in Agra.";
		Predicate<String> isf = n->{
			return n.contains("AST");
		};
		if(isf.test(s)) {
			System.out.println("string found");
		}else {
			System.out.println("string not found");
		}
	}

}
