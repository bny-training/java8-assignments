/**
 * 
 */
package main;

import java.util.Scanner;
import java.util.function.Predicate;


/**
 * @author shubham.kushwah
 *
 */
public class NumberOps {

	public boolean checkNum(int num) {
		Predicate<Integer> isArm = i->{
			int temp = i;
			double sum = 0;
			int len = count(i);
	        
	        while (temp != 0) {
	            int rem = temp % 10;
	            sum = sum + Math.pow(rem, len);
	            temp = temp / 10;
	        }
	        return sum==i;
		};
		return isArm.test(num);
	}
	public int count(int num) {
		
			int cnt = 0;
	        while (num != 0) {
	            cnt++;
	            num = num / 10;
	        }
	        return cnt;
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		NumberOps nop = new NumberOps();
		boolean ops = true;
		int num = 0;
		while(ops) {
			System.out.println("Enter Number Operation "+"1 Even "+"2 Odd "+
					"3 Armstrong "+"4 Palindrome "+"Any input to terminate");
			int opt = sc.nextInt();
			System.out.println("Enter Number");
			
		switch(opt){
		case 1:
		case 2:
			num = sc.nextInt();
			Predicate<Integer> checkEvenOdd = n->n%2==0;
			if(checkEvenOdd.test(num)) {
				System.out.println("Even Number");
			}else {
				System.out.println("Odd Number");
			}
			break;
		case 3:
			num = sc.nextInt();
			System.out.println("Armstrong number ::"+nop.checkNum(num));
			break;
		case 4:
			num = sc.nextInt();
			Predicate<Integer> isp = n->{
				return String.valueOf(n).equals(new StringBuilder(String.valueOf(n)).reverse().toString());
			};
			System.out.println("Number is Palindrome ::"+isp.test(num));
			break;
			default:
				ops = false;
				System.out.println("Terminated");
				break;
		}
	}
}

}
