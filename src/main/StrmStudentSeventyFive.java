/**
 * 
 */
package main;

import java.util.List;
import java.util.function.Predicate;

import model.Student;
import model.StudentHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmStudentSeventyFive {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		StudentHelper studentHelper = new StudentHelper();
		List<Student> lsList = studentHelper.getLstStudent();
		Predicate<Student> pred = st->{
			if(Integer.parseInt(st.getChemistry())>75 && 
					Integer.parseInt(st.getEnglish())>75 &&
					Integer.parseInt(st.getHindi())>75 &&
					Integer.parseInt(st.getMaths())>75 &&
					Integer.parseInt(st.getPhysics())>75) {
				return true;
			}else {
				return false;
			}
		};
		System.out.println("Students with more than 75% marks in all subjects");
		lsList.stream().filter(pred).forEach(System.out::println);

	
	}

}
