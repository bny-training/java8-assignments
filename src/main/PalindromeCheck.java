/**
 * 
 */
package main;

import java.util.Scanner;
import java.util.function.Predicate;

/**
 * @author shubham.kushwah
 *
 */

public class PalindromeCheck {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Integer res = sc.nextInt();
		Predicate<Integer> isp = n->{
			return String.valueOf(n).equals(new StringBuilder(String.valueOf(n)).reverse().toString());
		};
		System.out.println("Is Palindrome "+isp.test(res));
		
	}

}
