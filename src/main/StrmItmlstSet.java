/**
 * 
 */
package main;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import model.Item;
import model.ItemHelper;
import model.ItemLessAttr;

/**
 * @author shubham.kushwah
 *
 */
public class StrmItmlstSet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {



		ItemHelper itmHelper = new ItemHelper();
		List<Item> lst = itmHelper.getLst();
		Function<Item,ItemLessAttr> fn = (itm)->{
			ItemLessAttr itmLess = new ItemLessAttr();
			itmLess.setIname(itm.getIname());
			itmLess.setPrice(itm.getPrice());
			return itmLess;
		};
		//		 List converted to Set with required attributes from Item
		Set<ItemLessAttr> highPrice = lst.stream().map(fn).collect(Collectors.toSet());
		System.out.println("===Item Name :: Item Price===");
		highPrice.forEach(itm->System.out.println(itm.getIname()+"	: "+itm.getPrice()));

	}

}
