/**
 * 
 */
package main;

import java.util.Comparator;
import java.util.List;

import model.Item;
import model.ItemHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StreamItmHighPrice {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ItemHelper itmHelper = new ItemHelper();
		List<Item> lst = itmHelper.getLst();
		Comparator<Item> maxItmCmp = (itm1,itm2)->{
			if(itm1.getPrice() < itm2.getPrice()) {
				return -1;
			}else if(itm1.getPrice() > itm2.getPrice()) {
				return 1;
			}else {
				return 0;
			}
			
		} ;
		Item highPrice = lst.stream().max(maxItmCmp).get();
		System.out.println("Item with Highest Price :: "+highPrice);
	}

}
