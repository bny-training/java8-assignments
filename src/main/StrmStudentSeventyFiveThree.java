/**
 * 
 */
package main;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import model.Student;
import model.StudentHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmStudentSeventyFiveThree {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		StudentHelper studentHelper = new StudentHelper();
		List<Student> lsList = studentHelper.getLstStudent();
		CalculateStudent calculateStudent = new CalculateStudent();
		Comparator<Integer> cmp = (i1,i2)->{
			return i1>i2?-1:i1<i2?1:0;
		};
		
		Predicate<Student> pred = st->{
			List<Integer> lstMarks = calculateStudent.toList(st,cmp);
			if(lstMarks.get(0)>75 && lstMarks.get(1)>75 && lstMarks.get(2)>75) {
				return true;
			}else {
				return false;
			}
		};
		System.out.println("Students scored more than 75% in any 3 subjects");
		lsList.stream().filter(pred).forEach(System.out::println);

	
	}

}
