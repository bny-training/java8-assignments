/**
 * 
 */
package main;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import model.Student;
import model.StudentHelper;

/**
 * @author shubham.kushwah
 *
 */
public class StrmStudentForty {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		StudentHelper studentHelper = new StudentHelper();
		List<Student> lsList = studentHelper.getLstStudent();
		CalculateStudent calculateStudent = new CalculateStudent();
		Predicate<Student> pred = st->{
			return calculateStudent.calcPercentage(st)<40?true:false;
		};
		System.out.println("Students with less than 40% marks");
		lsList.stream().filter(pred).forEach(System.out::println);

	}

}
