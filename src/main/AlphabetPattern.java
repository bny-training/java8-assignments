/**
 * 
 */
package main;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author shubham.kushwah
 *
 */
public class AlphabetPattern {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		char ch = 'Z';
		int middleAlpha = (int)ch;
		int diff = 0;
//		 Converts Input numbers to character value
		Function<Integer, Character> fn = s->{
			return (char)Integer.parseInt(s.toString());
		};
//		 Providing rest of the pattern with consumer
		Consumer<Integer> cp = n->{
			if(n>0) {
				for(int l=0;l<(n*2-1);l++) {
					System.out.print(" ");
				}
			for(int k=middleAlpha-n;k>=65;k--) {
				System.out.print(fn.apply(k));	// applying function
			}
			}else {
				for(int k=middleAlpha-1;k>=65;k--) {
					System.out.print(fn.apply(k));
				}
			}
		};
		for(int i=middleAlpha;i>=65;i--) {
			for(int j=65;j<=i;j++) {
				System.out.print(fn.apply(j));
			}
			cp.accept(diff);	// Consumer called
			diff++;
			System.out.println();
		}
	}

}
